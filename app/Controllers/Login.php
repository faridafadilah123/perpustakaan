<?php 
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\Barang_model;

class Login extends Controller
{
    public function index()
    {
        $model = new Barang_model;
        $data['title']     = 'Data Barang';
        $data['getBarang'] = $model->getBarang();
        echo view('header_view', $data);
        echo view('login_view', $data);
        echo view('footer_view', $data);
    }

    public function login()
    {
        $model = new Login_model;
        $data = array(
            'username' => $this->request->getPost('username'),
            'password' => $this->request->getPost('password'),
        );
        $model->login($data);
        echo '<script>
                alert("Sukses login");
                window.location="'.base_url('barang').'"
            </script>';

    }
}